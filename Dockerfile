FROM jupyter/scipy-notebook:latest

COPY requirements.txt /tmp/
RUN conda install --yes --file /tmp/requirements.txt && \
    fix-permissions $CONDA_DIR && \
    fix-permissions /home/$NB_USER

RUN mv work iml-demos
COPY *.ipynb iml-demos/
COPY utilities iml-demos/utilities
USER root
RUN chmod -R 755 iml-demos

USER $NB_UID
