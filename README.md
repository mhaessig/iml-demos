# Intro to ML Notebooks

This provides a Docker container to run the demo Jupyter notebooks from the
Introduction to Machine Learning lecture in the spring semester 2020.

## Requirements
You need [docker](https://www.docker.com/) running on your machine and this repository.
This readme assumes you are on a Unix machine and that your user is in the
docker group. For Windows and Mac there are also desktop apps for running containers.

## Start the notebooks
To run the notebooks first build the container by running the following command
in the root of this repo
```bash
docker build -t iml-demos .
```
After building the container you can run it with
```bash
docker run -it -rm -p 8888:8888 iml-demos
```
Then open the link displayed on your terminal  in your browser.
